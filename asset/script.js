// Hamburger Icon animation

const hamburg = document.getElementById('nav-icon3');
// dropdown nav link
const dropdown = document.querySelector('.dropdown-list');
// console.log(hamburg.classList);
hamburg.addEventListener('click', () => {
  hamburg.classList.toggle('open');
  dropdown.classList.toggle('active');
});
